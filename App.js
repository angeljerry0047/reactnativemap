import React, { Component } from 'react';
import AppNavigator from './src/navigators/appNavigator';
import {
  View,
  AsyncStorage,
  StyleSheet,
  StatusBar,
  Platform,
  Dimensions
} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import SplashScreen from 'react-native-splash-screen';
let { width, height } = Dimensions.get('window');

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showRealApp: false
    };
    this._retrieveData();
  }
  
  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('app_status');
      if (value == 'true') {
        this.setState({
          showRealApp: true
        })
      }
      else {
        return;
      }

    } catch (error) {
    }
  };
  _onDone = () => {
    AsyncStorage.setItem('app_status', 'true');
    this.setState({ showRealApp: true });
  };
  _onSkip = () => {
    AsyncStorage.setItem('app_status', 'true');
    this.setState({ showRealApp: true });
  };
  render() {
    if (this.state.showRealApp) {
      return (
        <AppNavigator />
      );
    }
    else {
      setTimeout(()=>{this.setState({Guideshow: true})},500);
      SplashScreen.hide();
      return (
        <View style={styles.container}>
          <StatusBar barStyle="dark-content" hidden={true} backgroundColor="#7FCCF7" translucent={true} />
          <AppIntroSlider
            slides={slides}
            onDone={this._onDone}
            showSkipButton={true}
            onSkip={this._onSkip}
          />
        </View>
      );
    }
  }
}
const styles = StyleSheet.create({

  title: {
    fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium',
    fontSize: 30,
    fontWeight: 'bold',
    color: 'black',
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginTop: 16,
  },
  image: {
    position: 'absolute',
    top: 0,
    width: width * 0.9,
    height: height * 0.91,
  },
  text: {
    color: 'black',
    fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium',
  },
  container: {
    flex: 1,
  }
});
const slides = [
  {
    key: 's1',
    // text: 'Please set pin.If city was allowed, You can set wherever.',
    // title: 'Welcome to here.',
    titleStyle: styles.title,
    textStyle: styles.text,
    image: require('./src/img/page1.png'),
    imageStyle: styles.image,
    backgroundColor: 'rgba(211,211,211,0.8)',
  },
  {
    key: 's2',
    // title: 'Create Pin',
    titleStyle: styles.title,
    // text: 'Input Pin data(three images and title etc.)',
    image: require('./src/img/page2.png'),
    imageStyle: styles.image,
    textStyle: styles.text,
    backgroundColor: 'rgba(211,211,211,0.8)',
  },
  {
    key: 's3',
    // title: 'See Pin',
    titleStyle: styles.title,
    // text: 'Please click your like, support, contribute pin.',
    image: require('./src/img/page3-1.png'),
    imageStyle: styles.image,
    textStyle: styles.text,
    backgroundColor: 'rgba(211,211,211,0.8)',
  },
  {
    key: 's4',
    titleStyle: styles.title,
    textStyle: styles.text,
    // text: 'You can add your idea for pin.',
    image: require('./src/img/page3-2.png'),
    imageStyle: styles.image,
    backgroundColor: 'rgba(211,211,211,0.8)',
  },
  {
    key: 's5',
    // title: 'User Data',
    titleStyle: styles.title,
    image: require('./src/img/page4.png'),
    imageStyle: styles.image,
    textStyle: styles.text,
    backgroundColor: 'rgba(211,211,211,0.8)',
  }
];
console.disableYellowBox = true;
