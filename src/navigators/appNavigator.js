import { createStackNavigator,createAppContainer } from 'react-navigation';
import editPin from '../screens/editPin';
import viewPin from '../screens/viewPin';
import mapContainer from '../screens/mapContainer';
import realPin from '../screens/realPin';
import detailView from '../screens/detailView';
const app_Navigator = createStackNavigator({
    MainPage:{screen:mapContainer},
    Editpin: { screen: editPin },
    Viewpin: { screen: viewPin },
    Realpin: { screen: realPin},
    Detailview: {screen: detailView},
  });
  const AppNavigator = createAppContainer(app_Navigator);
  export default AppNavigator;