import React, { Component } from 'react';
import { 
    Image, 
    TouchableOpacity, 
    ScrollView, 
    Dimensions, 
    Platform, 
    StyleSheet, 
    TextInput, 
    Alert, 
    Text, 
    View ,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-picker';
import { StatusBar } from 'react-native'
import moment from 'moment';
import * as constant from "../utils/constant";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Spinner from 'react-native-loading-spinner-overlay';

let { screenWidth, screenHeight } = Dimensions.get('window');
const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu Button for dev menu',
});

const options = {
    title: 'Select Image',
    customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

export default class editPin extends Component {
    static navigationOptions = {
        title: null,
        header: null
    };

    constructor(props) {
        super(props)
        const lat_pin = this.props.navigation.getParam('lat_pin');
        const long_pin = this.props.navigation.getParam('long_pin');
        const user_name = this.props.navigation.getParam('user_name');
        const user_token = this.props.navigation.getParam('user_token');
        const user_id = this.props.navigation.getParam('user_id');
        this.state = {
            spinner: false,
            user_id: user_id,
            user_token: user_token,
            user_name: user_name,
            long_pin: long_pin,
            lat_pin: lat_pin,
            title_pin: '',
            description_pin: '',
            amount_pin: '',
            active_pin: '',
            image_pin1: null,
            image_pin2: null,
            image_pin3: null,
            names: [
                { 'name': 'Ben', 'id': 1 },
                { 'name': 'Susan', 'id': 2 },
                { 'name': 'Robert', 'id': 3 },
                { 'name': 'Mary', 'id': 4 },
                { 'name': 'Daniel', 'id': 5 },
                { 'name': 'Laura', 'id': 6 },
                { 'name': 'John', 'id': 7 },
                { 'name': 'Debra', 'id': 8 },
                { 'name': 'Aron', 'id': 9 },
                { 'name': 'Ann', 'id': 10 },
                { 'name': 'Steve', 'id': 11 },
                { 'name': 'Olivia', 'id': 12 }
            ]
        };
    }

    _getPhoto1() {
        ImagePicker.launchImageLibrary(options, (response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    image_pin1: source,
                    data: response.data
                });
            }
        });
    }
    _getPhoto2() {
        ImagePicker.launchImageLibrary(options, (response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    image_pin2: source,
                });
            }
        });
    }
    _getPhoto3() {
        ImagePicker.launchImageLibrary(options, (response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    image_pin3: source,
                });
            }
        });
    }
    async _createPin() {
        const { user_id } = this.state;
        const { user_name } = this.state;
        const { user_token } = this.state;
        const { lat_pin } = this.state;
        const { long_pin } = this.state;
        const { title_pin } = this.state;
        const { description_pin } = this.state;
        const { amount_pin } = this.state;
        const { active_pin } = this.state;
        pin_status = 1;
        like_number = 1;
        support_number = 1;
        let today = moment(new Date());
        let created_at = today.format('YYYY-MM-DD');
        let localUri1 = this.state.image_pin1.uri;
        let filename1 = localUri1.split('/').pop();
        let localUri2 = this.state.image_pin2.uri;
        let filename2 = localUri2.split('/').pop();
        let localUri3 = this.state.image_pin3.uri;
        let filename3 = localUri3.split('/').pop();
        let formData = new FormData();
        formData.append("pin_status", pin_status);
        formData.append("like_number", like_number);
        formData.append("support_number", support_number);
        formData.append("created_at", created_at);
        formData.append("title_pin", title_pin);
        formData.append("description_pin", description_pin);
        formData.append("amount_pin", amount_pin);
        formData.append("active_pin", active_pin);
        formData.append("lat_pin", lat_pin);
        formData.append("long_pin", long_pin);
        formData.append("user_name", user_name);
        formData.append("user_token", user_token);
        formData.append("user_id", user_id);
        formData.append('image_pin1', { uri: localUri1, name: filename1, type: `image/jpg` });
        formData.append('image_pin2', { uri: localUri2, name: filename2, type: `image/jpg` });
        formData.append('image_pin3', { uri: localUri3, name: filename3, type: `image/jpg` });
        try {
            fetch(`${constant.domain}/create_pin/`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data'
                },
                body: formData,
            }).then((response) => response.json())
                .then((res => {
                    if (res.status == 1) {
                        this.props.navigation.navigate('MainPage', { date: new Date() });
                        this.setState({
                            spinner: !this.state.spinner
                        })
                    }
                    else {
                        this.setState({
                            spinner: !this.state.spinner
                        })
                        Alert.alert(
                            '',
                            'Tilslut netværksfejl',
                            [
                                { text: 'OK', onPress: () => console.log('OK Pressed') },
                            ],
                            { cancelable: false }
                        )
                    }
                })
                ).done();
        }
        catch (e) {
            this.setState({
                spinner: !this.state.spinner
            })
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBar barStyle="dark-content" hidden={true} backgroundColor="#7FCCF7" translucent={true} />
                <View style={styles.tophead}>
                    <TouchableOpacity style={{ width: '13%', }} onPress={() => this.props.navigation.navigate('MainPage', { date: new Date() })}><Icon style={styles.topicon} name="chevron-left" size={25} color="#7FCCF7" /></TouchableOpacity>
                    <Text style={styles.toptitle}>Opret din idé</Text>
                </View>
                <View style={styles.horizontalcontainer}>
                    <ScrollView style={styles.horcontent} horizontal={true} showsHorizontalScrollIndicator={false}>
                        <View style={styles.horruler} /><View style={styles.horruler} />
                        <View style={styles.horruler} />
                        <View style={styles.item}><TouchableOpacity onPress={() => this._getPhoto1()}><Image style={styles.addimage} source={this.state.image_pin1 != null ? this.state.image_pin1 : require('../img/init.png')} /></TouchableOpacity></View>
                        <View style={styles.horruler} />
                        <View style={styles.item}><TouchableOpacity onPress={() => this._getPhoto2()}><Image style={styles.addimage} source={this.state.image_pin2 != null ? this.state.image_pin2 : require('../img/init.png')} /></TouchableOpacity></View>
                        <View style={styles.horruler} />
                        <View style={styles.item}><TouchableOpacity onPress={() => this._getPhoto3()}><Image style={styles.addimage} source={this.state.image_pin3 != null ? this.state.image_pin3 : require('../img/init.png')} /></TouchableOpacity></View>
                        <View style={styles.horruler} />
                        <View style={styles.horruler} />
                        <View style={styles.horruler} />
                        <View style={styles.horruler} />
                        <View style={styles.horruler} />
                    </ScrollView>
                </View>
                <View style={styles.verticalcontainer}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        
                    >
                            <View style={styles.vitem1}>
                                <TextInput 
                                 returnKeyType='done' 
                                maxLength={30} style={[{ fontStyle: 'italic' }, styles.btntxt]} placeholder="Skriv titel på din idé" onChangeText={(title_pin) => this.setState({ title_pin })}></TextInput>
                            </View>
                            <View style={styles.vitem2}>
                                <ScrollView showsVerticalScrollIndicator={false}>
                                    <TextInput  returnKeyType='done'  maxLength={500} multiline={true} style={[{ fontStyle: 'italic', margin: 25 }, styles.btntxt]} placeholder="Beskriv din idé" onChangeText={(description_pin) => this.setState({ description_pin })}></TextInput>
                                </ScrollView>
                            </View>
                            <View style={styles.vitem3}>
                                <Text style={styles.destxt}>Beløb jeg evt. vil bidrage med (Intet er bindende)</Text>
                            </View>
                            <View style={styles.vitem4}>
                                <TextInput   returnKeyType='done' style={styles.btntxt} placeholder="F.eks. 50 kr." onChangeText={(amount_pin) => this.setState({ amount_pin })}></TextInput>
                            </View>
                            <View style={styles.vitem5}>
                                <Text style={styles.destxt}>Antal dage ideen skal være aktiv (maks 365 dage)</Text>
                            </View>
                            <View style={styles.vitem6}>
                                <TextInput  returnKeyType='done'  style={styles.btntxt} placeholder="F.eks. 100 dage" onChangeText={(active_pin) => this.setState({ active_pin })}></TextInput>
                            </View>
                            <View style={styles.vitem7}>
                                <TouchableOpacity onPress={() => {
                                    this._createPin(); 
                                    this.setState({
                                        spinner: !this.state.spinner
                                    })
                                }} style={styles.firsttbtn}><Text style={styles.txttxt}>Opret idé</Text></TouchableOpacity>
                            </View>
                            <View style={{ height: 200 }}>
                            </View>
                    </ScrollView>
                </View>
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Indlæser...'}
                    textStyle={styles.spinnerTextStyle}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    spinnerTextStyle: {
        color: '#FFF'
    },
    vercontent1: {
        width: '80%',
        height: screenHeight - 190
    },
    vercontent2: {
        width: '80%',
        height: screenWidth - 190
    },
    vitem1: {
        // width: '80%',
        marginTop: 15,
        // backgroundColor: 'white',
        height: 40,
        borderRadius: 20,
        borderWidth: 2,
        borderColor: '#7FCCF7',
    },
    vitem2: {
        height: 100,
        marginTop: 25,
        // backgroundColor: 'white',
        borderRadius: 20,
        borderWidth: 2,
        borderColor: '#7FCCF7',
    },
    vitem3: {
        marginTop: 25,
        // backgroundColor: 'white',
    },
    vitem4: {
        height: 40,
        marginTop: 10,
        // backgroundColor: 'white',
        borderRadius: 20,
        borderWidth: 2,
        borderColor: '#7FCCF7',
    },
    vitem5: {
        marginTop: 25,
        // backgroundColor: 'white',
    },
    vitem6: {
        height: 40,
        marginTop: 10,
        // marginBottom: 60,
        // backgroundColor: 'white',
        alignItems: 'center',
        borderRadius: 20,
        borderWidth: 2,
        borderColor: '#7FCCF7',
    },
    vitem7: {
        marginTop: 50,
        backgroundColor: '#7FCCF7',
        alignItems: 'center',
        borderRadius: 20,

    },
    destxt: {
        paddingLeft: 15,
        color: 'black',
        fontSize: 13,
        fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light',
    },
    btntxt: {
        fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light',
        paddingTop: 8,
        fontSize: 16,
        paddingBottom: 8,
        textAlign: 'center',
        color: 'black'
    },
    firsttbtn: {
        width: '100%',
    },
    txttxt: {
        fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light',
        textAlign: 'center',
        color: 'white',
        paddingTop: 8,
        fontSize: 17,
        paddingBottom: 8,
    },
    container: {
        flex: 1
    },
    tophead: {
        // backgroundColor: '#1FF',
        flexDirection: 'row',
        width: '100%',
        height: 50,
        zIndex: 999,
        position: 'absolute',
        top: 0,
    },
    toptitle: {
        fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light',
        marginTop: 13,
        textAlign: 'center',
        fontSize: 20,
        color: 'black',
        width: '80%',
        marginLeft: -10,
    },
    topicon: {
        marginLeft: 15,
        marginTop: 15,
    },
    verticalcontainer: {
        width: '100%',
        position: 'absolute',
        top: 190,
        // backgroundColor: '#10F',
        alignItems: 'center',
    },
    horizontalcontainer: {
        position: 'absolute',
        top: 50,
        // backgroundColor: '#19F',
    },
    horcontent: {
        padding: 10,
        position: 'relative',
        height: 140,
        // width: '50%'
    },
    horruler: {
        width: 10,
    },
    item:
    {
        padding: 0,
        color: 'black',
        borderWidth: 2,
        borderColor: '#7FCCF7',
        borderRadius: 5,
        height: 120,
        width: 210,
    },
    addimage: {
        width: '100%',
        height: '100%',
    },
    item11: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 30,
        margin: 2,
        borderColor: '#2a4944',
        borderWidth: 1,
        backgroundColor: '#d2f7f1'
    }
});

console.disableYellowBox = true;
