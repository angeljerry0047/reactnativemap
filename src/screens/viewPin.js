import React, { Component } from 'react';
import {
    Platform,
    Alert,
    Text,
    ScrollView,
    StatusBar,
    View,
    StyleSheet,
    Dimensions,
    TouchableOpacity
} from 'react-native';
import ImageSlider from 'react-native-image-slider';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/Entypo';
import { TextInput } from 'react-native-gesture-handler';
import * as constant from "../utils/constant";
import { ShareDialog } from 'react-native-fbsdk';
import Dialog from 'react-native-popup-dialog';
import * as RNIap from 'react-native-iap';
import Spinner from 'react-native-loading-spinner-overlay';

let { width, height } = Dimensions.get('window');
var click_btn = 0;
const itemSkus = Platform.select({
    ios: [
        'com.cooni.point1000',
    ],
    android: [
        'com.example.coins100',
    ],
});
export default class viewPin extends Component {
    static navigationOptions = {
        title: null,
        header: null
    };
    constructor(props) {
        super(props)
        let like_pin = this.props.navigation.getParam('like_pin');
        let support_pin = this.props.navigation.getParam('support_pin');
        let like_number = this.props.navigation.getParam('like_number');
        let support_number = this.props.navigation.getParam('support_number');
        let lat_pin = this.props.navigation.getParam('lat_pin');
        let long_pin = this.props.navigation.getParam('long_pin');
        let pin_id = this.props.navigation.getParam('pin_id');
        let user_id = this.props.navigation.getParam('user_id');
        let title_pin = this.props.navigation.getParam('title_pin');
        let active_pin = this.props.navigation.getParam('active_pin');
        let amount_pin = this.props.navigation.getParam('amount_pin');
        let description_pin = this.props.navigation.getParam('description_pin');
        let user_name = this.props.navigation.getParam('user_name');
        let user_role = this.props.navigation.getParam('user_role');
        let user_token = this.props.navigation.getParam('user_token');
        let enter_user = this.props.navigation.getParam('enter_user');
        let image_pin1 = this.props.navigation.getParam('image_pin1');
        let image_pin2 = this.props.navigation.getParam('image_pin2');
        let image_pin3 = this.props.navigation.getParam('image_pin3');
        let pin_status = this.props.navigation.getParam('pin_status');
        let city_code = this.props.navigation.getParam('city_code');
        let shareLinkContent = {
            contentType: 'link',
            contentUrl: image_pin1,
            contentDescription: 'Facebook image sharing !'
        };
        this.state = {
            spinner: false,
            editable_payment: false,
            date_time: [],
            productList: [],
            receipt: '',
            availableItemsMessage: '',
            user_id: user_id,
            pin_id: pin_id,
            city_code: city_code,
            enter_user: enter_user,
            pin_status: pin_status,
            temp: 0,
            editabled: false,
            visible_inapp: false,
            visible_detail: false,
            device_type: Platform.OS === 'ios' ? 'iOS' : 'Android',
            shareLinkContent: shareLinkContent,
            user_role: user_role,
            user_token: user_token,
            lat_pin: lat_pin,
            long_pin: long_pin,
            images: [
                image_pin1,
                image_pin2,
                image_pin3,
            ],
            payment: 'De som har meldt sig som bidragsydere, kan betale til Marlene på MobilePay: 60270561Det er også muligt, at medbringe kontanter i løbet af de næste par møder.',
            destitle: title_pin,
            messaged1: 'Der vil være gratis suppe og saftevand til både de frivillige, samt dem som har bidraget rent økonomisk, efterfølgende.',
            messaged2: 'Hvis der er én, som kan tage en gravemaskine med, bedes du kontakte Jørgen på: 60270566',
            dessenten: description_pin,
            realbtn: pin_status == 2 ? styles.realbtn2 : styles.realbtn1,
            secondview: pin_status == 2 ? styles.content22 : styles.content21,
            firstview: pin_status == 2 ? styles.content12 : styles.content12,
            like_number: like_number,
            support_number: support_number,
            height_hc: styles.content1,
            height_ch: styles.sliderimg1,
            closestyle: styles.closebtn2,
            backbtn: styles.backbtn1,
            txttt: active_pin,
            amount_pin: amount_pin + '.kr',
            onetxt: like_pin != 1 ? 'Støt ideen' : 'Annuller støtte',
            thirdtxt: support_pin != 1 ? 'Meld som frivillig' : 'Afmeld Som frivillig',
            username: user_name,
        };
    }
    async componentDidMount() {
        this.getItems();
        this.get_date();
    }
    componentWillUnmount() {
        RNIap.endConnection();
    }
    goToNext = () => {
        try {
            fetch(`${constant.domain}/inapppurchase_freemium/`, {
                method: 'POST',
                header: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    user_token: this.state.user_token,
                    receipt: this.state.receipt,
                })
            })
                .then((response) => response.json())
                .then((res => {
                    if (res.status == 1) {
                        this.setState({
                            user_role: 'freemium',
                            spinner: false
                        })
                        switch (this.state.user_status) {
                            case 1:
                                this._detailView1();
                                break;
                            case 2:
                                this._detailView2();
                                break;
                            case 3:
                                this._detailView3();
                                break;
                            default:
                                break;
                        }
                    }
                    else if (res.status == 3) {
                        this.setState({
                            spinner: false
                        })
                        alert('Gemte ikke data!');
                    }
                    else {
                        this.setState({
                            spinner: false
                        })
                        Alert.alert(
                            '',
                            'Tilslut netværksfejl!',
                            [
                                { text: 'OK', onPress: () => console.log('OK Pressed') },
                            ],
                            { cancelable: true }
                        )
                    }
                })
                ).done();
        }
        catch (e) {
            this.setState({
                spinner: false
            })
        }
    }
    getItems = async () => {
        try {
            await RNIap.initConnection();
            const products = await RNIap.getProducts(itemSkus);
            this.setState({ items: products.productId });
        } catch (err) {
            console.warn(err);
        }
    }
    buyItem = async (sku) => {
        alert('buyItem: ' + sku);
        const purchase = await RNIap.buyProduct(sku);
        try {
            this.setState({ receipt: purchase.transactionReceipt }, () => this.goToNext());
        } catch (err) {
            alert(err.code, err.message);
            const subscription = RNIap.addAdditionalSuccessPurchaseListenerIOS(async (purchase) => {
                this.setState({ receipt: purchase.transactionReceipt }, () => this.goToNext());
                subscription.remove();
            });
        }
    }
    showAlert() {
        Alert.alert(
            'Velkommen til ', this.state.user_name,
            [
                { text: 'OK', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false },
        );

    }
    shareImageWithShareDialog() {
        var tmp = this;
        ShareDialog.canShow(this.state.shareLinkContent).then(
            function (canShow) {
                if (canShow) {
                    return ShareDialog.show(tmp.state.shareLinkContent);
                }
            }
        ).then(
            function (result) {
                if (result.isCancelled) {
                    alert('Del annulleret');
                } else {
                    alert('Del succes');
                }
            },
            function (error) {
                alert('Del mislykkes med fejl: ' + error);
            }
        );
    }
    _zoomoutView() {
        this.setState({
            height_hc: styles.content2,
            height_ch: styles.sliderimg2,
            closestyle: styles.closebtn1,
            backbtn: styles.backbtn2,
        });
    }
    _zoominView() {
        this.setState({
            height_hc: styles.content1,
            height_ch: styles.sliderimg1,
            closestyle: styles.closebtn2,
            backbtn: styles.backbtn1,
        });
    }
    show_pincode_dialog(val) {
        switch (val) {
            case 1:
                this.setState({
                    visible_pin_code: true,
                    dialog_status: 1
                })
                break;
            case 2:
                this.setState({
                    visible_pin_code: true,
                    dialog_status: 2
                })
                break;
            case 3:
                this.setState({
                    visible_pin_code: true,
                    dialog_status: 3
                })
                break;
            case 4:
                this.setState({
                    visible_pin_code: true,
                    dialog_status: 4
                })
                break;
            default:
                break;
        }
    }
    choose_function(val) {
        switch (val) {
            case 1:
                this._realize();
                break;
            case 2:
                this.add_date();
                break;
            case 3:
                this.add_payment();
                break;
            case 4:
                this.add_review();
                break;
            default:
                break;
        }
    }
    add_date() {
        if (this.state.pin_code == this.state.city_code) {
            this.setState({
                visible_insert: true,
                insert_status: 1,
                insert_title: 'Realisér nye data'
            })
        }
        else {
            alert('PIN-kodefejl!');
            this.setState({
                visible_insert: false
            })
        }
    }
    formatTime(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = ampm + '.' + hours + ':' + minutes;
        return "" + strTime;
    }
    formatDate(date) {
        return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
    }
    insert_function(val) {
        var now = new Date();
        var insert_date = this.formatDate(now);
        var insert_time = this.formatTime(now);
        if (val == 1) {
            try {
                fetch(`${constant.domain}/insert_date_time/`, {
                    method: 'POST',
                    header: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        user_id: this.state.user_id,
                        pin_id: this.state.pin_id,
                        note_txt: this.state.insert_data,
                        date_txt: insert_date,
                        time_txt: insert_time
                    })
                })
                    .then((response) => response.json())
                    .then((res => {
                        if (res.status == 1) {
                            this.componentDidMount();
                            this.setState({
                                spinner: false
                            })
                        }
                        else if (res.status == 3) {
                            this.setState({
                                spinner: false
                            })
                            alert('Gemte ikke data!');
                        }
                        else {
                            this.setState({
                                spinner: false
                            })
                            Alert.alert(
                                '',
                                'Tilslut netværksfejl!',
                                [
                                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                                ],
                                { cancelable: true }
                            )
                        }
                    })
                    ).done();
            }
            catch (e) {
                this.setState({
                    spinner: false
                })
            }

        }
        if (val == 2) {
            try {
                fetch(`${constant.domain}/edit_payment/`, {
                    method: 'POST',
                    header: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        user_id: this.state.user_id,
                        pin_id: this.state.pin_id,
                        edit_payment: this.state.insert_data
                    })
                })
                    .then((response) => response.json())
                    .then((res => {
                        if (res.status == 1) {
                            this.componentDidMount();
                            this.setState({
                                spinner: false
                            })
                        }
                        else if (res.status == 3) {
                            this.setState({
                                spinner: false
                            })
                            alert('Gemte ikke!');
                        }
                        else {
                            this.setState({
                                spinner: false
                            })
                            Alert.alert(
                                '',
                                'Tilslut netværksfejl!',
                                [
                                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                                ],
                                { cancelable: true }
                            )
                        }
                    })
                    ).done();
            }
            catch (e) {
                this.setState({
                    spinner: false
                })
            }
        }
        if (val == 3) {
            try {
                fetch(`${constant.domain}/insert_review/`, {
                    method: 'POST',
                    header: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        user_id: this.state.user_id,
                        pin_id: this.state.pin_id,
                        review_date: insert_date,
                        review_txt: this.state.insert_data
                    })
                })
                    .then((response) => response.json())
                    .then((res => {
                        if (res.status == 1) {
                            this.componentDidMount();
                            this.setState({
                                spinner: false
                            })
                        }
                        else if (res.status == 3) {
                            this.setState({
                                spinner: false
                            })
                            alert('Gemte ikke!!');
                        }
                        else {
                            this.setState({
                                spinner: false
                            })
                            Alert.alert(
                                '',
                                'Tilslut netværksfejl!',
                                [
                                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                                ],
                                { cancelable: true }
                            )
                        }
                    })
                    ).done();
            }
            catch (e) {
                this.setState({
                    spinner: false
                })
            }
        }
    }
    add_payment() {
        if (this.state.pin_code == this.state.city_code) {
            this.setState({
                visible_insert: true,
                insert_status: 2,
                insert_title: 'Rediger betaling'
            })
        }
        else {
            alert('PIN-kodefejl');
            this.setState({
                visible_insert: false
            })
        }

    }
    add_review() {
        if (this.state.pin_code == this.state.city_code) {
            this.setState({
                visible_insert: true,
                insert_status: 3,
                insert_title: 'Indsæt anmeldelse'
            })
        }
        else {
            alert('PIN-kodefejl');
            this.setState({
                visible_insert: false
            })
        }
    }
    get_date() {
        try {
            fetch(`${constant.domain}/get_date_time/`, {
                method: 'POST',
                header: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    user_id: this.state.user_id,
                    pin_id: this.state.pin_id
                })
            })
                .then((response) => response.json())
                .then((res => {
                    if (res.status == 1) {
                        this.setState({
                            spinner: false
                        })
                        var v1 = Object.values(res.data);
                        this.setState({
                            date_time: v1,
                        })
                        if (res.data[0].payment_method != '') {
                            this.setState({
                                payment: res.data[0].payment_method
                            })
                        }
                        else { return }
                    }
                    else {
                        this.setState({
                            spinner: false
                        })
                    }
                })
                ).done();
        }
        catch (e) {
            this.setState({
                spinner: false
            })
        }
    }
    show_date() {
        if (this.state.date_time == null) { return; }
        return (
            <View style={{ flexDirection: 'row' }}>
                {
                    this.state.date_time.map(item => {
                        if (item.note_txt == '') { return; }
                        return (
                            <View style={{ flexDirection: 'row' }}>
                                <View style={styles.item}>
                                    <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', marginTop: 5, fontSize: 12, textAlign: 'center', color: 'black' }}>{item.date_txt}</Text>
                                    <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', fontSize: 12, textAlign: 'center', color: 'black' }}>{item.time_txt}</Text>
                                    <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', margin: 5, fontSize: 12, textAlign: 'center', color: 'black', height: 60 }}>{item.note_txt}</Text>
                                </View>
                                <View style={styles.horruler} />
                            </View>
                        )
                    })
                }
            </View>
        );
    }
    show_review() {

        if (this.state.date_time == null) { return; }
        return (
            <View style={{ flexDirection: 'column' }}>
                {
                    this.state.date_time.map(item => {
                        if (item.review_txt == '') { return; }
                        return (
                            <View style={{ alignItems: 'center' }}>
                                <View style={{ width: '80%', borderBottomColor: '#7FCCF7', borderBottomWidth: 1, justifyContent: 'space-between', flexDirection: 'row', paddingTop: 5, paddingBottom: 5 }}>
                                    <Text numberOfLines={3} multiline={true} style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', textAlign: 'left', width: '65%', fontSize: 11, color: 'black' }} editable={false}>{item.review_txt}</Text>
                                    <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', textAlign: 'right', width: '35%', fontSize: 12, color: 'black' }} editable={false}>{item.review_date}</Text>
                                </View>
                            </View>
                        )
                    })
                }
            </View>
        );
    }


    _realize() {
        if (this.state.city_code !== this.state.pin_code) {
            alert('PIN-kodefejl, du har ikke oprettet denne pin!');
            return;
        }
        try {
            fetch(`${constant.domain}/status_pin/`, {
                method: 'POST',
                header: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    id: this.state.pin_id,
                    pin_status: 2,
                    user_token: this.state.user_token,
                    user_name: this.state.username,
                    device_type: this.state.device_type
                })
            })
                .then((response) => response.json())
                .then((res => {
                    if (res.status == 1) {
                        this.setState({
                            firstview: styles.content12,
                            realbtn: styles.realbtn2,
                            secondview: styles.content22,
                        });
                        setTimeout(() => { this.refs.scrollView.scrollTo(height * 3) }, 50);
                        this.showAlert();
                    }
                    else {
                        Alert.alert(
                            '',
                            'Tilslut netværksfejl!',
                            [
                                { text: 'OK', onPress: () => console.log('OK Pressed') },
                            ],
                            { cancelable: true }
                        )
                    }
                })
                ).done();
        }
        catch (e) {
        }

    }
    _delete_pin() {
        if (this.state.user_token !== this.state.enter_user) { alert('Du kan ikke slette denne knap, fordi du ikke har oprettet denne knap.'); return; }
        Alert.alert(
            'ADVARSEL:',
            'Virkelig Pin Delete?',
            [
                { text: 'Annuller', onPress: () => console.log('cancel Pressed') },
                { text: 'OK', onPress: () => { this.real_deletepin(); this.setState({ spinner: true }) } },
            ],
            { cancelable: false }
        )
    }
    real_deletepin() {
        try {
            fetch(`${constant.domain}/delete_pin/`, {
                method: 'POST',
                header: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    id: this.state.pin_id,
                    pin_status: 3
                })
            })
                .then((response) => response.json())
                .then((res => {
                    if (res.status == 1) {
                        this.props.navigation.navigate('MainPage', { date: new Date() });
                        this.setState({
                            firstview: styles.content11,
                            secondview: styles.content22,
                            spinner: false
                        });
                    }
                    else {
                        this.setState({ spinner: false });
                        Alert.alert(
                            '',
                            'Netværksforbindelse fejl',
                            [
                                { text: 'OK', onPress: () => console.log('OK Pressed') },
                            ],
                            { cancelable: true }
                        )
                    }
                })
                ).done();
        }
        catch (e) {
            this.setState({ spinner: false });
        }
    }
    like_up() {
        if (this.state.onetxt !== 'Annuller støtte') {
            try {
                fetch(`${constant.domain}/like_pin/`, {
                    method: 'POST',
                    header: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        pin_id: this.state.pin_id,
                        like_pin: 1,
                        user_id: this.state.user_id
                    })
                })
                    .then((response) => response.json())
                    .then((res => {
                        if (res.status == 1) {
                            this.setState({
                                onetxt: 'Annuller støtte',
                                like_number: parseInt(this.state.like_number) + 1,
                            });
                        }
                        else {
                            Alert.alert(
                                '',
                                'Tilslut netværksfejl.',
                                [
                                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                                ],
                                { cancelable: true }
                            )
                        }
                    })
                    ).done();
            }
            catch (e) {
            }
        }
        else {
            try {
                fetch(`${constant.domain}/like_pin/`, {
                    method: 'POST',
                    header: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        pin_id: this.state.pin_id,
                        like_pin: -1,
                        user_id: this.state.user_id
                    })
                })
                    .then((response) => response.json())
                    .then((res => {
                        if (res.status == 1) {
                            this.setState({
                                onetxt: 'Støt ideen',
                                like_number: parseInt(this.state.like_number) - 1,
                            });
                        }
                        else {
                            Alert.alert(
                                '',
                                'Tilslut netværksfejl.',
                                [
                                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                                ],
                                { cancelable: true }
                            )
                        }
                    })
                    ).done();
            }
            catch (e) {
            }
        }

    }
    support_up() {
        if (this.state.thirdtxt !== 'Meld som frivillig') {

            try {
                fetch(`${constant.domain}/support_pin/`, {
                    method: 'POST',
                    header: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        pin_id: this.state.pin_id,
                        support_pin: -1,
                        user_id: this.state.user_id
                    })
                })
                    .then((response) => response.json())
                    .then((res => {
                        if (res.status == 1) {
                            this.setState({
                                thirdtxt: 'Meld som frivillig',
                                support_number: parseInt(this.state.support_number) - 1,
                            });
                        }
                        else {
                            Alert.alert(
                                '',
                                'Tilslut netværksfejl.',
                                [
                                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                                ],
                                { cancelable: true }
                            )
                        }
                    })
                    ).done();
            }
            catch (e) {
            }
        }
        else {

            try {
                fetch(`${constant.domain}/support_pin/`, {
                    method: 'POST',
                    header: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        pin_id: this.state.pin_id,
                        support_pin: 1,
                        user_id: this.state.user_id
                    })
                })
                    .then((response) => response.json())
                    .then((res => {
                        if (res.status == 1) {
                            this.setState({
                                thirdtxt: 'Afmeld Som frivillig',
                                support_number: parseInt(this.state.support_number) + 1,
                            });
                        }
                        else {
                            Alert.alert(
                                '',
                                'Tilslut netværksfejl.',
                                [
                                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                                ],
                                { cancelable: true }
                            )
                        }
                    })
                    ).done();
            }
            catch (e) {
            }
        }
    }
    onSomeEvent() {
        this.refs.scrollView.scrollTo(0);
        this.setState({
            firstview: styles.content12,
            secondview: styles.content21,
        });
    }
    _detailView1() {
        if (this.state.user_role == 'freemium') {
            try {
                fetch(`${constant.domain}/get_detail_data/`, {
                    method: 'POST',
                    header: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        pin_id: this.state.pin_id,
                    })
                })
                    .then((response) => response.json())
                    .then((res => {
                        if (res.status == 1) {
                            this.props.navigation.navigate('Detailview', { user_status: 1, names1: res.data[0].names1, names2: res.data[0].names2, names3: res.data[0].names3 });
                        }
                        else {
                            Alert.alert(
                                '',
                                'Tilslut netværksfejl.',
                                [
                                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                                ],
                                { cancelable: true }
                            )
                        }
                    })
                    ).done();
            }
            catch (e) {
            }

        }
        else {
            this.setState({
                visible_detail: true,
            })
        }
    }
    calcu() {
        this.setState({
            amount_pin: '',
        })
    }
    _detailView2() {
        if (this.state.user_role == 'freemium') {
            try {
                fetch(`${constant.domain}/get_detail_data/`, {
                    method: 'POST',
                    header: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        pin_id: this.state.pin_id,
                    })
                })
                    .then((response) => response.json())
                    .then((res => {
                        if (res.status == 1) {
                            this.props.navigation.navigate('Detailview', { user_status: 2, names1: res.data[0].names1, names2: res.data[0].names2, names3: res.data[0].names3 });
                        }
                        else {
                            Alert.alert(
                                '',
                                'Tilslut netværksfejl.',
                                [
                                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                                ],
                                { cancelable: true }
                            )
                        }
                    })
                    ).done();
            }
            catch (e) {
            }
        }
        else {
            this.setState({
                visible_detail: true,
            })
        }
    }
    _detailView3() {
        if (this.state.user_role == 'freemium') {
            try {
                fetch(`${constant.domain}/get_detail_data/`, {
                    method: 'POST',
                    header: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        pin_id: this.state.pin_id,
                    })
                })
                    .then((response) => response.json())
                    .then((res => {
                        if (res.status == 1) {
                            this.props.navigation.navigate('Detailview', { user_status: 3, names1: res.data[0].names1, names2: res.data[0].names2, names3: res.data[0].names3 });
                        }
                        else {
                            Alert.alert(
                                '',
                                'Tilslut netværksfejl.',
                                [
                                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                                ],
                                { cancelable: true }
                            )
                        }
                    })
                    ).done();
            }
            catch (e) {
            }
        }
        else {
            this.setState({
                visible_detail: true,
            })
        }
    }
     amount_add = async() => {
        var real_amount = parseInt(this.state.temp) + parseInt(this.state.additional_amount);
        try {
            await fetch(`${constant.domain}/contribute_pin/`, {
                method: 'POST',
                header: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    pin_id: this.state.pin_id,
                    contribute_pin: parseInt(this.state.amount_pin),
                    user_id: this.state.user_id,
                    amount_pin: real_amount,
                })
            })
                .then((response) => response.json())
                .then((res => {
                    if (res.status == 1) {
                        this.setState({
                            temp: 0,
                            visible_amount: false,
                            amount_pin: real_amount,
                            spinner: false
                        })
                    }
                    else {
                        this.setState({
                            spinner: false
                        })
                        Alert.alert(
                            '',
                            'Tilslut netværksfejl.',
                            [
                                { text: 'OK', onPress: () => console.log('OK Pressed') },
                            ],
                            { cancelable: true }
                        )
                    }
                })
                ).done();
        }
        catch (e) {

        }
    }
    _sumup = () => {
        this.setState({
            visible_amount: true,
            temp: this.state.amount_pin
        });
    }
    finish_project() {
        if (this.state.user_token !== this.state.enter_user) { alert('Du kan ikke slette denne knap, fordi du ikke har oprettet denne knap.'); return; }
        Alert.alert(
            'ADVARSEL:',
            'Virkelig Pin Delete?',
            [
                { text: 'Annuller', onPress: () => console.log('cancel Pressed') },
                {
                    text: 'OK', onPress: () => {
                        Alert.alert(
                            'ADVARSEL:',
                            'Virkelig Pin Delete?',
                            [
                                { text: 'Annuller', onPress: () => console.log('cancel Pressed') },
                                { text: 'OK', onPress: () => { this.real_deletepin(); this.setState({ spinner: true }) } },
                            ],
                            { cancelable: false }
                        )
                    }
                },
            ],
            { cancelable: false }
        )

    }
    render() {
        const { items } = this.state;
        return (
            <View style={styles.container}>
                <StatusBar barStyle="dark-content" hidden={true} backgroundColor="#7FCCF7" translucent={true} />
                <Dialog
                    containerStyle={{ flex: 1 }}
                    overlayBackgroundColor='rgba(0,0,0,0.7)'
                    visible={this.state.visible_insert}
                    onTouchOutside={() => {
                        this.setState({ visible_insert: false });
                    }}
                >
                    <View style={{ width: 300, height: 130, backgroundColor: 'rgba(238, 241, 253,0.7)' }}>
                        <View style={{ flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                            <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', marginLeft: 15, marginTop: 5, textAlign: 'left', fontSize: 25, color: 'rgb(0,0,0)' }}>{this.state.insert_title}</Text>
                        </View>
                        <ScrollView style={{ height: 50 }}>
                            <TextInput returnKeyType='done' multiline={true} maxLength={500} style={{ marginTop: 10, marginRight: 15, padding: 0, backgroundColor: 'rgba(0,0,255,0.1)', marginLeft: 15, fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', fontSize: 15 }} onChangeText={(insert_data) => { this.setState({ insert_data }) }}></TextInput>
                        </ScrollView>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginRight: 15 }}>
                            <TouchableOpacity onPress={() => {
                                this.insert_function(this.state.insert_status);
                                this.setState({
                                    visible_insert: false,
                                    spinner: true,
                                });
                            }}>
                                <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', padding: 10, textAlign: 'center', fontSize: 20, color: 'rgb(0,0,0)' }}>Gem      </Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({ visible_insert: !this.state.visible_insert })}>
                                <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', padding: 10, textAlign: 'center', fontSize: 20, color: 'rgb(0,0,0)' }}>Annuller </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Dialog>
                <Dialog
                    overlayBackgroundColor='rgba(0,0,0,0.7)'
                    visible={this.state.visible_pin_code}
                    onTouchOutside={() => {
                        this.setState({ visible_pin_code: false });
                    }}
                >
                    <View style={{ width: 250, height: 130, backgroundColor: 'rgba(238, 241, 253,0.7)' }}>
                        <View style={{ flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                            <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', marginLeft: 15, marginTop: 5, textAlign: 'left', fontSize: 25, color: 'rgb(0,0,0)' }}>Realisér PIN-kode</Text>
                        </View>
                        <TextInput returnKeyType='done' keyboardType='numeric' style={{ marginTop: 10, marginRight: 15, padding: 0, backgroundColor: 'rgba(0,0,255,0.1)', marginLeft: 15, fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', fontSize: 15 }} onChangeText={(pin_code) => { this.setState({ pin_code }) }}></TextInput>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginRight: 15 }}>
                            <TouchableOpacity onPress={() => { this.choose_function(this.state.dialog_status); this.setState({ visible_pin_code: false }) }}>
                                <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', padding: 10, textAlign: 'center', fontSize: 20, color: 'rgb(0,213,0)' }}>Realisér      </Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({ visible_pin_code: !this.state.visible_pin_code })}>
                                <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', padding: 10, textAlign: 'center', fontSize: 20, color: 'rgb(213,0,0)' }}>Annuller </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Dialog>
                <Dialog
                    overlayBackgroundColor='rgba(0,0,0,0.7)'
                    visible={this.state.visible_amount}
                    onTouchOutside={() => {
                        this.setState({ visible_amount: false });
                    }}
                >
                    <View style={{ width: 250, height: 130, backgroundColor: 'rgba(238, 241, 253,0.7)' }}>
                        <View style={{ flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                            <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', marginLeft: 15, marginTop: 5, textAlign: 'left', fontSize: 25, color: 'rgb(0,0,0)' }}>Input amount!</Text>
                        </View>
                        <TextInput returnKeyType='done' keyboardType='numeric' style={{ marginTop: 10, marginRight: 15, padding: 0, backgroundColor: 'rgba(0,0,255,0.1)', marginLeft: 15, fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', fontSize: 15 }} onChangeText={(additional_amount) => { this.setState({ additional_amount }) }}></TextInput>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginRight: 15 }}>
                            <TouchableOpacity onPress={() => { this.amount_add(); this.setState({ visible_amount: false,spinner: true }) }}>
                                <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', padding: 10, textAlign: 'center', fontSize: 20, color: 'rgb(0,213,0)' }}>OK      </Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({ visible_pin_code: !this.state.visible_pin_code })}>
                                <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', padding: 10, textAlign: 'center', fontSize: 20, color: 'rgb(213,0,0)' }}>Annuller </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Dialog>
                <Dialog
                    overlayBackgroundColor='rgba(0,0,0,0.7)'
                    visible={this.state.visible_detail}
                    onTouchOutside={() => {
                        this.setState({ visible_detail: false });
                    }}
                >
                    <View style={{ width: 300, height: 150, backgroundColor: 'rgba(238, 241, 253,0.7)' }}>
                        <View style={{ flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                            <Icon style={{ marginLeft: 15, marginTop: 12 }} name="warning" size={25} color="red" />
                            <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', marginLeft: 15, marginTop: 5, textAlign: 'left', fontSize: 25, color: 'rgb(0,0,0)' }}>Ikke-Freemium</Text>
                        </View>
                        <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', marginLeft: 15, marginTop: 5, textAlign: 'left', fontSize: 20, color: 'rgb(0,0,0)' }}>Vil du købe freemium?</Text>
                        <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', marginLeft: 15, marginTop: 0, textAlign: 'left', fontSize: 20, color: 'rgb(0,0,0)' }}>Hvis ja, klik på Ja.</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginRight: 15 }}>
                            <TouchableOpacity onPress={() => this.setState({ visible_detail: false, visible_inapp: true })}>
                                <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', padding: 10, textAlign: 'center', fontSize: 20, color: 'rgb(0,213,0)' }}>Ja     </Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({ visible_detail: !this.state.visible_detail })}>
                                <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', padding: 10, textAlign: 'center', fontSize: 20, color: 'rgb(213,0,0)' }}>Ingen </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Dialog>
                <Dialog
                    overlayBackgroundColor='rgba(0,0,0,0.9)'
                    visible={this.state.visible_inapp}
                    onTouchOutside={() => {
                        this.setState({ visible_inapp: false });
                    }}
                >
                    <View style={{ width: 340, height: 150, backgroundColor: 'rgba(238, 241, 253,0.7)' }}>
                        <View style={{ alignItems: 'center' }}>
                            <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', marginLeft: 15, marginTop: 10, textAlign: 'left', fontSize: 30, color: 'rgb(0,0,0)' }}>Bekræft dit i app køb?</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', borderBottomColor: 'gray', borderBottomWidth: 1 }}>
                            <Icon style={{ marginLeft: 5, marginRight: 5, marginTop: 9 }} name="money" size={35} color="rgb(255, 191, 0)" />
                            <Text style={{ marginLeft: 5, marginRight: 5, marginTop: 5, textAlign: 'left', fontSize: 35, color: 'green', fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', }}>Freemium:</Text>
                            <Text style={{ marginLeft: 5, marginRight: 5, marginTop: 5, textAlign: 'left', fontSize: 35, color: 'green', fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', }}>18.kr</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <TouchableOpacity onPress={() => { this.buyItem(items); }} style={{ marginLeft: 40 }}>
                                <Text style={{ padding: 10, textAlign: 'center', fontSize: 20, color: 'rgb(0,213,0)', fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', }}>Købe    </Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({ visible_inapp: false })} style={{ marginRight: 40 }}>
                                <Text style={{ padding: 10, textAlign: 'center', fontSize: 20, color: 'rgb(213,0,0)', fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light', }}>Annuller</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Dialog>
                <ScrollView showsVerticalScrollIndicator={false} ref="scrollView">
                    <View style={this.state.firstview}>
                        <ScrollView showsVerticalScrollIndicator={false}>
                            <View style={this.state.backbtn}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('MainPage', { date: new Date() })}>
                                    <Icon name="chevron-left" size={30} color="white" />
                                </TouchableOpacity>
                            </View>
                            <View style={this.state.closestyle}>
                                <TouchableOpacity onPress={() => this._zoominView()}>
                                    <Icon name="close" size={30} color="#7FCCF7" />
                                </TouchableOpacity>
                            </View>
                            <ImageSlider
                                images={this.state.images}
                                loopBothSides
                                style={this.state.height_ch}
                                onPress={() => this._zoomoutView()}
                                customButtons={(position, move) => (
                                    <View style={styles.buttons}>
                                        {this.state.images.map((image, index) => {
                                            return (
                                                <TouchableOpacity
                                                    key={index}
                                                    underlayColor="#ccc"
                                                    onPress={() => move(index)}
                                                    style={styles.button}
                                                >
                                                    <Icon name="circle" size={16} color="white" style={position === index && styles.buttonSelected} />
                                                </TouchableOpacity>
                                            );
                                        })}
                                    </View>
                                )}
                            />

                            <View style={styles.contenttop}>
                                <View style={styles.facecon}>
                                    <TouchableOpacity onPress={this.shareImageWithShareDialog.bind(this)}>
                                        <Icon2 name="facebook-with-circle" size={25} color="black" />
                                    </TouchableOpacity>
                                    <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', marginLeft: 8, color: 'black' }}>Del ide´</Text>
                                </View>
                                <View style={styles.spilte}></View>
                                <View style={styles.disdate}>
                                    <View style={styles.circled}>
                                        <TouchableOpacity style={{ backgroundColor: '#7FCCF7', width: 25, height: 25, borderRadius: 100, }}>
                                            <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', marginTop: 4, textAlign: 'center', color: 'white', fontSize: 11, fontWeight: 'bold' }}>{this.state.txttt}</Text>
                                        </TouchableOpacity>
                                        <Text style={{ marginLeft: 8, color: 'black' }}>dage tilbage</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{ alignItems: 'center', marginTop: -5, }}>
                                <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', color: 'black', textAlign: 'center', fontSize: 20, width: '100%' }}>
                                    {this.state.destitle}
                                </Text>
                            </View>
                            <View style={{ alignItems: 'center', marginTop: 10, }}>
                                <Text style={{ color: 'black', fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', fontSize: 16, width: '80%', textAlign: 'left' }}>
                                    {this.state.dessenten}
                                </Text>
                            </View>
                            <View>
                                <Text style={{ textAlign: 'center', marginTop: 10, fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', fontSize: 12, color: 'black', fontStyle: 'italic' }}>
                                    {this.state.username}
                                </Text>
                            </View>
                            <View style={{ alignItems: 'center', marginTop: 15, }}>
                                <Text style={{ width: '80%', color: 'black', fontSize: 13, fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', textAlign: 'left' }}>Antal beboere som støtter denne ide´:</Text>
                                <View style={{ width: '80%', justifyContent: 'space-between', flexDirection: 'row', marginTop: 5 }}>
                                    <TouchableOpacity onPress={() => { this._detailView1(); this.setState({ user_status: 1 }) }} style={{ borderRadius: 100, borderColor: '#7FCCF7', borderWidth: 1, width: '35%' }}>
                                        <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', textAlign: 'center', color: 'black', width: '100%', paddingBottom: 10, paddingTop: 10, fontSize: 15 }}>{this.state.like_number}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.like_up()} style={{ borderRadius: 100, backgroundColor: '#7FCCF7', width: '60%' }}>
                                        <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', width: '100%', textAlign: 'center', color: 'white', paddingBottom: 10, paddingTop: 10, fontSize: 15, fontWeight: 'bold' }}>{this.state.onetxt}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ alignItems: 'center', marginTop: 15, }}>
                                <Text style={{ width: '80%', color: 'black', fontSize: 13, fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', textAlign: 'left' }}>Antal frivillige til denne ide´:</Text>
                                <View style={{ width: '80%', justifyContent: 'space-between', flexDirection: 'row', marginTop: 5 }}>
                                    <TouchableOpacity onPress={() => { this._detailView3(); this.setState({ user_status: 3 }) }} style={{ borderRadius: 100, borderWidth: 1, borderColor: '#7FCCF7', width: '35%' }}>
                                        <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', textAlign: 'center', color: 'black', width: '100%', paddingBottom: 10, paddingTop: 10, fontSize: 15 }}>{this.state.support_number}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.support_up()} style={{ borderRadius: 100, backgroundColor: '#7FCCF7', width: '60%' }}>
                                        <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', width: '100%', textAlign: 'center', color: 'white', paddingBottom: 10, paddingTop: 10, fontSize: 15, fontWeight: 'bold' }}>{this.state.thirdtxt}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ alignItems: 'center', marginTop: 15 }}>
                                <Text style={{ width: '80%', color: 'black', fontSize: 13, fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', textAlign: 'left' }}>Bidrag fra byens beboere til denne ide´:</Text>
                                <View style={{ width: '80%', justifyContent: 'space-between', flexDirection: 'row', marginTop: 5 }}>
                                    {/* <TouchableOpacity onPress={this._detailView2.bind(this)} style={{ borderRadius: 100, borderColor: '#7FCCF7', borderWidth: 1, textAlign: 'center', width: '35%', paddingBottom: 8, paddingTop: 8 }}>
                                        <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', textAlign: 'center', color: 'black', width: '100%', paddingBottom: 10, paddingTop: 10, fontSize: 15 }}>{this.state.support_number}</Text>
                                        <TextInput  returnKeyType='done' keyboardType='numeric' onFocus={() => this.calcu()} editable={this.state.editabled} onChangeText={(amount_pin) => this.setState({ amount_pin })} style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', borderRadius: 100, textAlign: 'center', color: 'black', width: '100%', paddingBottom: 0, paddingTop: 0, fontSize: 15 }} value={this.state.amount_pin}></TextInput>
                                    </TouchableOpacity> */}
                                    <TouchableOpacity onPress={() => { this._detailView2(); this.setState({ user_status: 2 }) }} style={{ borderRadius: 100, borderWidth: 1, borderColor: '#7FCCF7', width: '35%' }}>
                                        <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', textAlign: 'center', color: 'black', width: '100%', paddingBottom: 10, paddingTop: 10, fontSize: 15 }}>{this.state.amount_pin}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={()=>this._sumup()} style={{ borderRadius: 100, backgroundColor: '#7FCCF7', width: '60%' }}>
                                        <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', width: '100%', textAlign: 'center', color: 'white', paddingBottom: 10, paddingTop: 10, fontSize: 15, fontWeight: 'bold' }}>Tilbyd et bidrag</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ height: 50 }}>
                            </View>
                            <View style={this.state.realbtn}>
                                <View style={{ width: '60%' }}>
                                    <TouchableOpacity onPress={() => this.show_pincode_dialog(1)} style={{ borderRadius: 100, backgroundColor: '#7FCCF7', width: '100%' }}><Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', width: '100%', textAlign: 'center', color: 'white', paddingBottom: 10, paddingTop: 10, fontSize: 15, fontWeight: 'bold' }}>Realisér idé</Text></TouchableOpacity>
                                </View>
                                <View style={{ height: 30 }}></View>
                                <TouchableOpacity onPress={() => this._delete_pin()}>
                                    <Text style={{ textDecorationLine: "underline", textDecorationStyle: 'solid', textAlign: 'center', color: 'black', fontSize: 15, fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium' }}>Slet idé</Text>
                                </TouchableOpacity>
                                <View style={{ height: 60 }}></View>
                            </View>
                        </ScrollView>
                    </View>
                    <View style={{ height: 10 }}>
                    </View>
                    <View style={this.state.secondview}>
                        <ScrollView showsVerticalScrollIndicator={false}>
                            <View style={{ marginTop: 20 }}>
                                <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', fontSize: 17, color: 'black', textAlign: 'center', fontWeight: 'bold' }}>
                                    Tillykke! Denne idé er realiseret.
                                </Text>
                                <Text style={{ marginTop: 10, fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', fontSize: 12, color: 'black', textAlign: 'center' }}>
                                    I kan nu planlægge og aftale detaljer
                                </Text>
                                <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', fontSize: 12, color: 'black', textAlign: 'center' }}>
                                    vedrørende udførelsen af ideen.
                                </Text>
                                <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', fontSize: 12, color: 'black', textAlign: 'center' }}>
                                    Held og lykke.
                                </Text>
                            </View>
                            <View style={{ marginTop: 25, alignItems: 'center' }}>
                                <View style={{ justifyContent: 'space-between', flexDirection: 'row', width: '80%' }}>
                                    <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', fontSize: 17, color: 'black', textAlign: 'left', fontWeight: 'bold' }}>Tider og datoer:</Text>
                                    <TouchableOpacity style={{ marginTop: 5 }} onPress={() => this.show_pincode_dialog(2)}>
                                        <Icon name="plus-circle" size={18} color="#7FCCF7" />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ alignItems: 'center' }}>
                                <ScrollView ref="scrollViewH" style={styles.horcontent} horizontal={true} showsHorizontalScrollIndicator={false}>
                                    {this.show_date()}
                                </ScrollView>
                            </View>
                            <View style={{ marginTop: 6, alignItems: 'center' }}>
                                <View style={{ justifyContent: 'space-between', flexDirection: 'row', width: '80%' }}>
                                    <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', fontSize: 17, color: 'black', textAlign: 'left', fontWeight: 'bold' }}>Betaling:</Text>
                                    <TouchableOpacity style={{ marginTop: 5 }} onPress={() => this.show_pincode_dialog(3)}>
                                        <Icon name="plus-circle" size={18} color="#7FCCF7" />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ alignItems: 'center' }}>
                                <View style={{ width: '80%', marginTop: 4, justifyContent: 'space-between', flexDirection: 'row' }}>
                                    <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', textAlign: 'left', width: '80%', fontSize: 13, color: 'black' }}>{this.state.payment}</Text>
                                </View>
                            </View>
                            <View style={{ marginTop: 20, alignItems: 'center' }}>
                                <View style={{ justifyContent: 'space-between', flexDirection: 'row', width: '80%' }}>
                                    <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', fontSize: 17, color: 'black', textAlign: 'left', fontWeight: 'bold' }}>Øvrige meddelelser:</Text>
                                    <TouchableOpacity style={{ marginTop: 5 }} onPress={() => this.show_pincode_dialog(4)}>
                                        <Icon name="plus-circle" size={18} color="#7FCCF7" />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                {this.show_review()}
                            </View>
                            <View style={{ alignItems: 'center', marginTop: 15, marginBottom: 15, }}>
                                <View style={{ width: '60%' }}>
                                    <TouchableOpacity onPress={() => this.finish_project()} style={{ borderRadius: 100, backgroundColor: '#7FCCF7', width: '100%' }}>
                                        <Text style={{ fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium', width: '100%', textAlign: 'center', color: 'white', paddingBottom: 10, paddingTop: 10, fontSize: 15, fontWeight: 'bold' }}>Afslut projektet</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </ScrollView>
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Indlæser...'}
                    textStyle={styles.spinnerTextStyle}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    spinnerTextStyle: {
        color: '#FFF'
    },
    realbtn1: {
        alignItems: 'center',
    },
    realbtn2: {
        display: 'none',
        alignItems: 'center',
    },
    horcontent: {
        paddingTop: 8,
        paddingBottom: 8,
        height: 140,
        width: '80%'
    },
    horruler: {
        width: 15,
    },
    item:
    {
        padding: 0,
        color: 'black',
        borderWidth: 2,
        borderColor: '#7FCCF7',
        borderRadius: 20,
        height: 115,
        width: 100,
    },
    topicon: {
        marginLeft: 10,
        marginTop: 15,
    },
    toptitle: {
        marginTop: 10,
        fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium',
        textAlign: 'center',
        fontSize: 23,
        color: 'white',
        width: '100%',
        marginLeft: -10,
    },
    tophead: {
        position: 'relative',
        top: 0,
        backgroundColor: '#7FCCF7',
        flexDirection: 'row',
        zIndex: 999,
        height: 50,
    },
    content12: {
        width: '100%',
    },
    content11: {
        display: 'none',
        width: '100%',
    },

    content21: {
        display: 'none',
        width: '100%',
    },
    content22: {
        width: '100%',
    },
    bottomdelete: {
        marginTop: 20,
        marginBottom: 40,
    },
    contenttop: {
        height: '7%',
        flexDirection: 'row',
        justifyContent: 'center',
        width: '100%',
    },
    facecon: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    spilte: {
        width: '8%',
        height: '100%',
    },
    circled: {
        flexDirection: 'row',
        width: '100%',
        height: '100%',
        alignItems: 'center',
    },
    disdate: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    button: {
        fontSize: 16,
        color: 'green',
        paddingRight: 7,
        paddingLeft: 7,
    },
    buttons: {
        color: '#7FCCF7',
        flexDirection: 'row',
        justifyContent: 'center',
        position: 'absolute',
        width: '100%',
        top: '91%',
    },
    buttonSelected: {
        color: '#7FCCF7',
        fontSize: 16,

    },
    backbtn1: {
        position: 'absolute',
        top: 10,
        zIndex: 222,
        width: 30,
        height: 30,
        marginLeft: 10,
        borderRadius: 15,
    },
    backbtn2: {
        position: 'absolute',
        top: 10,
        width: 30,
        height: 30,
        marginLeft: 10,
        borderRadius: 15,
    },
    sliderimg1: {
        width: '100%',
        height: Dimensions.get('window').height / 100 * 42,
    },

    sliderimg2: {
        width: '100%',
        height: Dimensions.get('window').height,
        zIndex: 200,
    },
    closebtn1: {
        position: 'absolute',
        top: 20,
        right: 20,
        zIndex: 222,
    },
    closebtn2: {
        position: 'absolute',
        top: 20,
        right: 20,
    },
    btn: {
        alignSelf: 'center',
        backgroundColor: '#00c40f',
        borderRadius: 0,
        borderWidth: 0,
    },
    txt: {
        color: 'white',
    },
});
console.disableYellowBox = true;
