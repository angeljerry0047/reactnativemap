import React, { Component } from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  Alert,
  View,
  PermissionsAndroid,
  TextInput, Platform,
  TouchableOpacity,
  Linking,
  KeyboardAvoidingView
} from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps'
import { Avatar } from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';
import { StatusBar } from 'react-native';
import RNFetchBlob from 'react-native-fetch-blob';
import * as constant from "../utils/constant";
import Geocoder from 'react-native-geocoder';
import firebase from 'react-native-firebase';
import { ShareDialog } from 'react-native-fbsdk';

import Spinner from 'react-native-loading-spinner-overlay';
import {
  shareOnFacebook,
  shareOnTwitter,
} from 'react-native-social-share';
import SplashScreen from 'react-native-splash-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
let { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 56.1722;
const LONGITUDE = 10.1739;
const LATITUDE_DELTA = 1.9212;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const options = {
  title: 'Vælg et billede',
  takePhotoButtonTitle: 'Tag et billede',
  chooseFromLibraryButtonTitle: 'Vælg fra galleri',
  quality: 1
};

export default class MapContainer extends Component {
  static navigationOptions = {
    title: null,
    header: null,
  };
  constructor(props) {
    super(props);
    const shareLinkContent = {
      contentType: 'link',
      contentUrl: 'https://itunes.apple.com/us/app/mydesk/id1233604818?ls=1&mt=8%20-',
      contentDescription: 'Facebook sharing is link!'
    };
    this.state = {
      maptype: 'standard',
      typeicon: require('../img/satellite.png'),
      shareLinkContent: shareLinkContent,
      cities: [],
      long_name: null,
      user_name: null,
      data: null,
      markers: [],
      markers1: [],
      changed: false,
      editavatar: styles.disabledavatar,
      chlatitude: LATITUDE,
      chlongitude: LONGITUDE,
      editableuser: require('../img/checkmark.png'),
      avatarSource: null,
      initname: null,
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      spinner: false
    }
    this.requestCameraPermission();
    this.requestLocationPermission();
    this.handlePress = this.handlePress.bind(this);
  }
  componentWillReceiveProps() {
    if (this.props.date !== null) {
      this._getpin();
    }
    else {
      return;
    }
  }
  shareLinkWithShareDialog() {
    // var tmp = this;
    // ShareDialog.canShow(this.state.shareLinkContent).then(
    //   function (canShow) {
    //     if (canShow) {
    //       return ShareDialog.show(tmp.state.shareLinkContent);
    //     }
    //   }
    // ).then(
    //   function (result) {
    //     if (result.isCancelled) {
    //       alert('Del annulleret!');
    //     } else {
    //       alert('Del succes!');
    //       // alert('Share success with postId: ' + result.postId);
    //     }
    //   },
    //   function (error) {
    //     alert('Del mislykkes med fejl: ' + error);
    //   }
    // );
    shareOnFacebook({
      'link': 'https://artboost.com/',
      'imagelink': 'https://artboost.com/apple-touch-icon-144x144.png',
      //or use image

    },
      (results) => {
        alert(results)
      }
    );

  }
  async  requestLocationPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': 'Cool Photo App Camera Permission',
          'message': 'Cool Photo App needs access to your camera ' +
            'so you can take awesome pictures.'
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the location")
      } else {
        console.log("location permission denied")
      }
    } catch (err) {
      console.warn(err)
    }
  }
  async  requestCameraPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          'title': 'Cool Photo App Camera Permission',
          'message': 'Cool Photo App needs access to your camera ' +
            'so you can take awesome pictures.'
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the camera")
      } else {
        console.log("Camera permission denied")
      }
    } catch (err) {
      console.warn(err)
    }
  }

  _getPhoto() {
    ImagePicker.launchImageLibrary(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        const source = { uri: response.uri };
        this.setState({
          avatarSource: source,
          data: response.data
        });
      }
    });
  }
  async componentDidMount() {
    SplashScreen.hide();
    this.checkPermission();
  }

  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  async getToken() {
    firebase.messaging().getToken()
      .then(fcmToken => {
        if (fcmToken) {
          this.setState({ token: fcmToken })
          this._get_user_data();
        } else {
        }
      });

  }

  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      this.getToken();
    } catch (error) {
      console.log('permission rejected');
    }
  }
  _get_user_data() {
    try {
      fetch(`${constant.domain}/get_user_data/`, {
        method: 'POST',
        header: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          user_token: this.state.token,
        })
      })
        .then((response) => response.json())
        .then((res => {
          if (res.status == 1) {
            var user_avatar = { uri: `${constant.domain}` + res.data[0].user_avatar }
            this.setState({
              avatarSource: user_avatar,
              user_name: res.data[0].user_name,
              user_token: res.data[0].user_token,
              user_role: res.data[0].user_role,
              user_id: res.data[0].id,
              changed: false,
              editavatar: styles.disabledavatar,
              editableuser: require('../img/checkmark.png'),
            })
            this._getpin();
            this._getcity();
          }
          else {
            this._getpin();
            this._getcity();

          }
        })
        ).done();
    }
    catch (e) {
    }
  }
  _getcity() {
    try {
      fetch(`${constant.domain}/get_city/`, {
        method: 'POST',
        header: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username: this.state.user_name,
        })
      })
        .then((response) => response.json())
        .then((res => {
          var shareLinkContent = {
            contentType: 'link',
            contentUrl: Platform.OS === 'ios' ? res.data[0].apple_link : res.data[0].google_link,
            contentDescription: 'Facebook sharing is link!'
          };
          if (res.status == 1) {
            var result = [];
            for (var i in res.data)
              result.push([res.data[i].city_name]);
            this.setState({
              cities: result,
              shareLinkContent: shareLinkContent
            })
          }
          else if (res.status == 3) {
            Alert.alert(
              '',
              'Beklager Denne by er endnu ikke medlem af Idébyen.',
              [
                { text: 'OK', onPress: () => console.log('OK Pressed') },
                { text: 'Bliv medlem', onPress: () => Linking.openURL('https://denmark.dk') }
              ],
              { cancelable: false }
            )
          }
          else {
            Alert.alert(
              '',
              'Tilslut netværksfejl',
              [
                { text: 'OK', onPress: () => console.log('OK Pressed') },
              ],
              { cancelable: false }
            )
          }
        })
        ).done();
    }
    catch (e) {
    }
  }
  _getpin() {
    try {
      fetch(`${constant.domain}/get_pin/`, {
        method: 'POST',
        header: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          user_token: this.state.token,
        })
      })
        .then((response) => response.json())
        .then((res => {
          if (res.status == 1) {
            var tempmaker = []
            for (var i = 0; i < res.data.length; i++) {
              tempmaker.push({
                coordinates: {
                  latitude: parseFloat(res.data[i].lat_pin),
                  longitude: parseFloat(res.data[i].long_pin),
                },
                pin_status: parseInt(res.data[i].pin_status)
              })
            }
            this.setState({
              markers1: Object.values(tempmaker)
            })
          }
          else if (res.status == 3) {
            // Alert.alert(
            //   '',
            //   'Der er ingen registreret pin',
            //   [
            //     { text: 'OK', onPress: () => console.log('OK Pressed') },
            //   ],
            //   { cancelable: false }
            // )
          }
          else {
            // Alert.alert(
            //   '',
            //   'Tilslut netværksfejl',
            //   [
            //     { text: 'OK', onPress: () => console.log('OK Pressed') },
            //   ],
            //   { cancelable: false }
            // )
          }
        })
        ).done();
    }
    catch (e) {
    }
  }
  async _getName() {
    if (this.state.changed !== true) {
      this.setState({
        changed: true,
        editavatar: styles.enabledavatar,
        editableuser: require('../img/001-edit.png'),
      });
    } else {
      this.setState({
        spinner: true
      })
      await RNFetchBlob.fetch('POST', `${constant.domain}/user_register/`, {
        Authorization: "Bearer access-token",
        otherHeader: "foo",
        'Content-Type': 'multipart/form-data',
      }, [
          { name: 'user_avatar', filename: 'image.png', type: 'image/png', data: this.state.data },
          { name: 'user_name', data: this.state.user_name },
          { name: 'user_token', data: this.state.token },
        ]).then((response) => response.json())
        .then((resp) => {
          if (resp.status == 1) {
            this._get_user_data();
            this.setState({
              changed: false,
              editavatar: styles.disabledavatar,
              editableuser: require('../img/checkmark.png'),
              spinner: false
            });
          }
          else if (resp.status == 3) {
            this.setState({
              spinner: false
            })
            alert('Tilslut netværksfejl');
          }
        }).catch((err) => {
          this.setState({
            spinner: false
          })
          alert(err);
        })
    }
  }

  _gotoMe() {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }
        });
      },
      (error) => console.log(error.message),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
    this.watchID = navigator.geolocation.watchPosition(
      position => {
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }
        });
      }
    );
  }
  onFocus() {
    this.setState({
      backgroundColor: 'cyan'
    })
  }
  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }
  _onMarkerPress = (evt) => {
    let lat_pin1 = evt.nativeEvent.coordinate.latitude;
    let long_pin1 = evt.nativeEvent.coordinate.longitude;
    Geocoder.fallbackToGoogle('AIzaSyBkGvJqnfLw2p2vhZmoHd_DE1lSc3b4PMg');
    var NY = {
      lat: evt.nativeEvent.coordinate.latitude,
      lng: evt.nativeEvent.coordinate.longitude
    };
    Geocoder.geocodePosition(NY).then(res => {
      var city_long_name = '' + JSON.stringify(res[0].formattedAddress);
      let lat_pin2 = '' + lat_pin1;
      let long_pin2 = '' + long_pin1;
      if (this.state.user_name == null) {
        alert('Først registrer brugernavn og avatar');
        return;
      }
      try {
        fetch(`${constant.domain}/pin_data/`, {
          method: 'POST',
          header: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            lat_pin: lat_pin2,
            long_pin: long_pin2,
            city_long_name: city_long_name,
            user_id: this.state.user_id
          })
        })
          .then((response) => response.json())
          .then((res => {
            if (res.status == 1) {
              this.props.navigation.navigate('Viewpin', {
                city_code: res.data[0].city_code,
                pin_id: res.data[0].id,
                user_id: this.state.user_id,
                lat_pin: res.data[0].lat_pin,
                long_pin: res.data[0].long_pin,
                like_pin: res.data[0].like_pin,
                support_pin: res.data[0].support_pin,
                title_pin: res.data[0].title_pin,
                created_at: res.data[0].created_at,
                user_name: res.data[0].user_name,
                user_role: this.state.user_role,
                user_token: res.data[0].user_token,
                enter_user: this.state.user_token,
                amount_pin: res.data[0].amount_pin,
                active_pin: res.data[0].active_pin,
                description_pin: res.data[0].description_pin,
                like_number: res.data[0].like_number,
                support_number: res.data[0].support_number,
                pin_status: res.data[0].pin_status,
                image_pin1: `${constant.domain}` + res.data[0].image_pin1,
                image_pin2: `${constant.domain}` + res.data[0].image_pin2,
                image_pin3: `${constant.domain}` + res.data[0].image_pin3,
              });
            }
            else if (res.status == 3) {
              Alert.alert(
                '',
                'Tilslut netværksfejl',
                [
                  { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: false }
              )
            }
          })
          ).done();
      }
      catch (e) {
      }
    })
  }
  handlePress(e) {
    if (this.state.user_name == null) { return 0; }
    Geocoder.fallbackToGoogle('AIzaSyBkGvJqnfLw2p2vhZmoHd_DE1lSc3b4PMg');
    var NY = {
      lat: e.nativeEvent.coordinate.latitude,
      lng: e.nativeEvent.coordinate.longitude
    };
    var coordinates = e.nativeEvent.coordinate;
    Geocoder.geocodePosition(NY).then(res => {
      var long_name1 = '' + JSON.stringify(res[0].formattedAddress);
      for (var i in this.state.cities) {
        if (long_name1.includes(this.state.cities[i])) {
          flag = 1;
          this.props.navigation.navigate('Editpin', {
            lat_pin: NY.lat,
            long_pin: NY.lng,
            user_name: this.state.user_name,
            user_token: this.state.token,
            user_id: this.state.user_id
          });
          this.setState({
            markers: [
              ...this.state.markers,
              {
                coordinate: coordinates,
              }
            ]
          });
          break;
        }
        flag = 0;
      }
      if (flag == 0) {
        Alert.alert(
          '',
          'Beklager Denne by er endnu ikke medlem af Idébyen.',
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
            { text: 'Bliv medlem', onPress: () => Linking.openURL('https://denmark.dk') }
          ],
          { cancelable: false }
        )
      }
    })
      .catch(err => console.log(err));
  }
  switch_maptype() {
    if (this.state.maptype == 'standard') {
      this.setState({
        maptype: 'satellite',
        typeicon: require('../img/drawing.png')
      })
    }
    else {
      this.setState({
        maptype: 'standard',
        typeicon: require('../img/satellite.png')
      })
    }
  }
  render() {
    let { maptype } = this.state;
    let { typeicon } = this.state;
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <View style={styles.container}>
          <StatusBar barStyle="dark-content" hidden={true} backgroundColor="#7FCCF7" translucent={true} />
          <View style={styles.topmenu}>
            <TouchableOpacity style={styles.runbtn} onPress={() => this._gotoMe()}>
              <Image style={{ width: 25, height: 25, marginTop: 8 }} source={require('../img/002-navigation.png')} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.facebtn1} onPress={this.shareLinkWithShareDialog.bind(this)}>
              <Image style={{ width: 25, height: 25, marginTop: 8 }} source={require('../img/003-facebook-logo-button.png')} />
            </TouchableOpacity>
          </View>
          <MapView
            mapType={maptype}
            provider={PROVIDER_GOOGLE}
            style={styles.mapview}
            initialRegion={this.state.region}
            region={this.state.region}
            onPress={(e) => this.handlePress(e)}
            showsCompass={false}
            showsMyLocationButton={false}
            showsUserLocation={true}
          >
            {this.state.markers1.map((marker) => {
              if (marker.pin_status == 3) { return }
              return (
                <MapView.Marker
                  coordinate={marker.coordinates}
                  onPress={(evt) => this._onMarkerPress(evt)}
                >
                  <View>
                    <Image style={styles.marker} source={marker.pin_status == 1 ? require('../img/placeholder1.png') : require('../img/placeholder2.png')} />
                  </View>
                </MapView.Marker>
              )
            })}
          </MapView>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', position: 'absolute', bottom: '10%', width: '100%' }}>
            <Text style={styles.instruction}>Klik på kortet, der hvor du ønsker at placere din idé</Text>
          </View>
          <TouchableOpacity onPress={() => this.switch_maptype()} style={styles.switchmap}><Image style={{ borderColor: 'white', borderWidth: 2, flex: 1, width: 40, height: 40 }} source={typeicon} /></TouchableOpacity>
          <View style={styles.useredit}>
            <View style={styles.userimage}>
              <TouchableOpacity onPress={() => this._getPhoto()}>
                <Avatar
                  size="small"
                  rounded
                  editable={this.state.changed}
                  source={this.state.avatarSource}
                  activeOpacity={0.7}
                />
              </TouchableOpacity>
              <View style={this.state.editavatar}></View>
            </View>
            <View style={styles.username}>

              <TextInput value={this.state.user_name}

                onFocus={() => this.onFocus()}
                onChangeText={(user_name) => this.setState({ user_name })}
                maxLength={20} editable={this.state.changed}
                selectTextOnFocus={this.state.changed}
                style={styles.usertext}
                placeholder="Patrick Pelle Rasmussen"
                returnKeyType='done'
              >
              </TextInput>

            </View>
            <View style={styles.userbtn}>
              <TouchableOpacity onPress={() => this._getName()}><Image style={{ width: 15, height: 15 }} source={this.state.editableuser} /></TouchableOpacity>
            </View>
          </View>
          <Spinner
            visible={this.state.spinner}
            textContent={'Indlæser...'}
            textStyle={styles.spinnerTextStyle}
          />
        </View>
      </KeyboardAvoidingView>
    );

  }
}

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: 'white'
  },
  title: {
    fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium',
    fontSize: 30,
    fontWeight: 'bold',
    color: 'black',
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginTop: 16,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  mapview: {
    flex: 1,
    flexDirection: 'column',
  },
  useredit: {
    height: 50,
    flexDirection: 'row',
    backgroundColor: 'white',
    justifyContent: 'center'
  },
  username: {
    width: '60%',
    height: '100%',
  },
  usertext: {
    width: '100%',
    textAlign: 'center',
    fontSize: 16,
    color: 'black',
    height: '100%',
    fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light',
  },
  userbtn: {
    width: '20%',
    paddingTop: 19,

  },
  enabledavatar: {
    height: 50,
    width: 50,
    position: 'absolute',
    bottom: 5,
    left: 40,
    zIndex: -100,
  },
  disabledavatar: {
    height: 50,
    width: 50,
    position: 'absolute',
    bottom: 5,
    left: 40,
    zIndex: 100,
  },
  userimage: {
    width: '20%',
    paddingTop: 9,
    alignItems: 'flex-end',
  },
  instruction: {
    color: 'black',
    fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light',
    fontStyle: 'italic',
    fontSize: 11.5,
    textAlign: 'center',
  },
  switchmap: {
    position: 'absolute',
    left: 5,
    bottom: 75,
    zIndex: 999,
    backgroundColor: 'rgba(55,55,255,0.5)',
  },
  topmenu: {
    flexDirection: 'row',
    flex: 1,
    position: 'absolute',
    top: 0,
    padding: 2,
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: 'rgba(55,255,255,0)',
    zIndex: 999,
    width: '100%'
  },
  runbtn: {
    backgroundColor: 'rgba(255,255,105,0)',
    zIndex: 999,
    alignItems: 'center',
    width: 40,
    height: 40,
    borderRadius: 20
  },
  facebtn1: {
    alignItems: 'flex-end',
    backgroundColor: 'rgba(255,55,105,0)',
    alignItems: 'center',
    width: 40,
    height: 40,
    borderRadius: 20
  },
  text: {
    color: "#FFF",
    fontWeight: "bold"
  },
  marker: {
    height: 50,
    width: 50,
    borderRadius: 50 / 2,
    overflow: 'hidden',
    backgroundColor: 'rgba(0,122,255,0)',
    borderWidth: 0,
    borderColor: 'rgba(0,112,255,0.3)',
    alignItems: 'center',
    justifyContent: 'center'
  },
  radius: {
    height: 20,
    width: 20,
    borderWidth: 3,
    borderColor: 'white',
    overflow: 'hidden',
    backgroundColor: '#007AFF'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  image: {
    position: 'absolute',
    top: 0,
    width: width * 0.9,
    height: height * 0.91,
  },
  text: {
    color: 'black',
    fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Medium',
  }
});
const slides = [
  {
    key: 's1',
    // text: 'Please set pin.If city was allowed, You can set wherever.',
    // title: 'Welcome to here.',
    titleStyle: styles.title,
    textStyle: styles.text,
    image: require('../img/page1.png'),
    imageStyle: styles.image,
    backgroundColor: 'rgba(211,211,211,0.8)',
  },
  {
    key: 's2',
    // title: 'Create Pin',
    titleStyle: styles.title,
    // text: 'Input Pin data(three images and title etc.)',
    image: require('../img/page2.png'),
    imageStyle: styles.image,
    textStyle: styles.text,
    backgroundColor: 'rgba(211,211,211,0.8)',
  },
  {
    key: 's3',
    // title: 'See Pin',
    titleStyle: styles.title,
    // text: 'Please click your like, support, contribute pin.',
    image: require('../img/page3-1.png'),
    imageStyle: styles.image,
    textStyle: styles.text,
    backgroundColor: 'rgba(211,211,211,0.8)',
  },
  {
    key: 's4',
    titleStyle: styles.title,
    textStyle: styles.text,
    // text: 'You can add your idea for pin.',
    image: require('../img/page3-2.png'),
    imageStyle: styles.image,
    backgroundColor: 'rgba(211,211,211,0.8)',
  },
  {
    key: 's5',
    // title: 'User Data',
    titleStyle: styles.title,
    image: require('../img/page4.png'),
    imageStyle: styles.image,
    textStyle: styles.text,
    backgroundColor: 'rgba(211,211,211,0.8)',
  }
];
console.disableYellowBox = true;