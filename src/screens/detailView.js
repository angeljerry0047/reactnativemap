import React, { Component } from 'react';
import { Platform, StatusBar, TouchableOpacity, ScrollView, StyleSheet, Text, Dimensions, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { List, ListItem, Avatar } from 'react-native-elements';
import * as constant from "../utils/constant";

let { width, height } = Dimensions.get('window');
export default class detailView extends Component {
    static navigationOptions = {
        title: null,
        header: null
    };
    constructor(props) {
        super(props)
        let user_status = this.props.navigation.getParam('user_status');
        let names1 = this.props.navigation.getParam('names1');
        let names2 = this.props.navigation.getParam('names2');
        let names3 = this.props.navigation.getParam('names3');
        this.state = {
            names1: names1,
            names2: names2,
            names3: names3,
            user_status: user_status,
            txt1: styles.txt11,
            txt2: styles.txt22,
            txt3: styles.txt32,
            firstpage: styles.firstpage1,
            secondpage: styles.secondpage2,
            thirdpage: styles.thirdpage2,
        };
    }
    alertItemName = (item) => {
        alert(item.name)
    }
    componentDidMount() {
        switch (this.state.user_status) {
            case 1:
                this.setState({
                    txt1: styles.txt11,
                    txt2: styles.txt22,
                    txt3: styles.txt32,
                    firstpage: styles.firstpage1,
                    secondpage: styles.secondpage2,
                    thirdpage: styles.thirdpage2,
                });
                break;
            case 2:
                this.setState({
                    txt1: styles.txt12,
                    txt2: styles.txt21,
                    txt3: styles.txt32,
                    firstpage: styles.firstpage2,
                    secondpage: styles.secondpage1,
                    thirdpage: styles.thirdpage2,
                });
                break;
            case 3:
                this.setState({
                    txt1: styles.txt12,
                    txt2: styles.txt22,
                    txt3: styles.txt31,
                    firstpage: styles.firstpage2,
                    secondpage: styles.secondpage2,
                    thirdpage: styles.thirdpage1,
                });
                break;
        }
    }
    _tabChange(e) {
        switch (e) {
            case 1:
                this.setState({
                    txt1: styles.txt11,
                    txt2: styles.txt22,
                    txt3: styles.txt32,
                    firstpage: styles.firstpage1,
                    secondpage: styles.secondpage2,
                    thirdpage: styles.thirdpage2,
                });
                break;
            case 2:
                this.setState({
                    txt1: styles.txt12,
                    txt2: styles.txt21,
                    txt3: styles.txt32,
                    firstpage: styles.firstpage2,
                    secondpage: styles.secondpage1,
                    thirdpage: styles.thirdpage2,
                });
                break;
            case 3:
                this.setState({
                    txt1: styles.txt12,
                    txt2: styles.txt22,
                    txt3: styles.txt31,
                    firstpage: styles.firstpage2,
                    secondpage: styles.secondpage2,
                    thirdpage: styles.thirdpage1,
                });
                break;
        }
    }
    renderRow({ item }) {
        return (
            <ListItem
                roundAvatar
                title={item.name}
                subtitle={item.amount}
                avatar={{ uri: `${constant.domain}` + item.avatar_url }}
                style={{ flexDirection: 'column' }}>
                <View style={{ height: 10, width: '100%', backgroundColor: '#7FCCF7' }}></View>
            </ListItem>

        )
    }
    renderitem1() {
        if (this.state.names1 == null) { return }
        return (
            <View>
                {
                    this.state.names1.map((item, index) => {
                        if (this.state.names1 != null) {
                            return (
                                <View
                                    key={item.id}
                                    style={styles.listcontainer}
                                >
                                    <View style={{ justifyContent: 'space-between', flexDirection: 'row', width: '100%' }}>
                                        <Avatar
                                            size="small"
                                            rounded
                                            source={{ uri: `${constant.domain}` + item.avatar_url }}
                                        />
                                        <Text style={styles.username}>
                                            {item.name}
                                        </Text>
                                        <Text style={styles.useramount}>
                                            {item.amount}.kr
                                    </Text>
                                    </View>
                                    <View style={{ backgroundColor: '#7FCCF7', height: 1, width: '100%', marginTop: 20 }}></View>
                                </View>
                            )
                        }
                    })
                }
            </View>
        )
    }
    renderitem2() {
        if (this.state.names2 == null) { return }
        return (
            <View>
                {
                    this.state.names2.map((item, index) => {
                        if (this.state.names2 != null) {
                            return (
                                <View
                                    key={item.id}
                                    style={styles.listcontainer}
                                >
                                    <View style={{ justifyContent: 'space-between', flexDirection: 'row', width: '100%' }}>
                                        <Avatar
                                            size="small"
                                            rounded
                                            source={{ uri: `${constant.domain}` + item.avatar_url }}
                                        />
                                        <Text style={styles.username}>
                                            {item.name}
                                        </Text>
                                        <Text style={styles.useramount}>
                                            {item.amount}.kr
                                </Text>
                                    </View>
                                    <View style={{ backgroundColor: '#7FCCF7', height: 1, width: '100%', marginTop: 20 }}></View>
                                </View>
                            )
                        }
                    })
                }
            </View>
        )
    }
    renderitem3() {
        if (this.state.names3 == null) { return }
        return (
            <View>
                {
                    this.state.names3.map((item, index) => {
                        if (this.state.names3 != null) {
                            return (
                                <View
                                    key={item.id}
                                    style={styles.listcontainer}
                                >
                                    <View style={{ justifyContent: 'space-between', flexDirection: 'row', width: '100%' }}>
                                        <Avatar
                                            size="small"
                                            rounded
                                            source={{ uri: `${constant.domain}` + item.avatar_url }}
                                        />
                                        <Text style={styles.username}>
                                            {item.name}
                                        </Text>
                                        <Text style={styles.useramount}>
                                            {item.amount}.kr
                            </Text>
                                    </View>
                                    <View style={{ backgroundColor: '#7FCCF7', height: 1, width: '100%', marginTop: 20 }}></View>
                                </View>
                            )
                        }
                    })
                }
            </View>
        )
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBar barStyle="dark-content" hidden={true} backgroundColor="#7FCCF7" translucent={true} />
                <View style={styles.tophead}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{marginLeft: 10,}}><Icon style={styles.topicon} name="chevron-left" size={25} color="white" /></TouchableOpacity>
                    <Text style={styles.toptitle}>Oversigt</Text>
                </View>
                <View style={styles.content}>
                    <View style={this.state.firstpage} >
                        <ScrollView showsVerticalScrollIndicator={false}>
                            {this.renderitem1()}
                        </ScrollView>
                    </View>
                    <View style={this.state.secondpage} >
                        <ScrollView showsVerticalScrollIndicator={false}>
                            {this.renderitem2()}
                        </ScrollView>
                    </View>
                    <View style={this.state.thirdpage} >
                        <ScrollView showsVerticalScrollIndicator={false}>
                            {this.renderitem3()}
                        </ScrollView>
                    </View>
                </View>
                <View style={styles.footer}>
                    <TouchableOpacity onPress={() => this._tabChange(1)} style={styles.btn}><Text style={this.state.txt1}>Støtte</Text></TouchableOpacity>
                    <TouchableOpacity onPress={() => this._tabChange(2)} style={styles.btn}><Text style={this.state.txt2}>Bidrag</Text></TouchableOpacity>
                    <TouchableOpacity onPress={() => this._tabChange(3)} style={styles.btn}><Text style={this.state.txt3}>Frivillig</Text></TouchableOpacity>
                </View>

            </View>

        );
    }
}

const styles = StyleSheet.create({
    username: {
        fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light',
        fontSize: 20,
        marginLeft: 40,
        marginTop: 5,
        color: 'black',
        width: width * 0.3
    },
    useramount: {
        fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light',
        fontSize: 15,
        marginLeft: 40,
        marginTop: 10,
        color: 'black'
    },
    listcontainer: {
        padding: 10,
        marginTop: 3,
        // backgroundColor: '#d9f9b1',
        alignItems: 'center',
    },
    txt11: {
        paddingTop: 10,
        color: '#7FCCF7',
        backgroundColor: 'white',
        textAlign: 'center',
        width: '100%',
        height: '100%',
        borderBottomLeftRadius: 100,
        borderTopLeftRadius: 100,
        borderWidth: 2,
        borderColor: '#7FCCF7',
    },
    txt12: {
        paddingTop: 10,
        color: 'white',
        backgroundColor: '#7FCCF7',
        textAlign: 'center',
        width: '100%',
        height: '100%',
        borderBottomLeftRadius: 100,
        borderTopLeftRadius: 100,
    },
    txt21: {
        paddingTop: 10,
        color: '#7FCCF7',
        backgroundColor: 'white',
        textAlign: 'center',
        width: '100%',
        height: '100%',
        borderWidth: 2,
        borderColor: '#7FCCF7',
    },
    txt22: {
        paddingTop: 10,
        color: 'white',
        backgroundColor: '#7FCCF7',
        textAlign: 'center',
        width: '100%',
        height: '100%',
    },
    txt31: {
        paddingTop: 10,
        color: '#7FCCF7',
        backgroundColor: 'white',
        textAlign: 'center',
        width: '100%',
        height: '100%',
        borderBottomRightRadius: 100,
        borderTopRightRadius: 100,
        borderWidth: 2,
        borderColor: '#7FCCF7',
    },
    txt32: {
        paddingTop: 10,
        color: 'white',
        backgroundColor: '#7FCCF7',
        textAlign: 'center',
        width: '100%',
        height: '100%',
        borderBottomRightRadius: 100,
        borderTopRightRadius: 100,
    },
    btn: {
        marginTop: 20,
        width: '25%',
        height: 40,

    },
    footer: {
        position: 'absolute',
        bottom: 0,
        height: 80,
        flexDirection: 'row',
        justifyContent: 'center',
        width: '100%'
    },
    content: {
        width: '100%',
        top: 50,
        position: 'absolute',
        backgroundColor: 'white',
        height: height - 130,
        alignItems: 'center',
        // backgroundColor: 'cyan',
    },
    firstpage1: {
        marginTop: 15,
        height: '100%',
        width: '80%',
        // backgroundColor: 'cyan',
        // zIndex: 100,
    },
    secondpage1: {
        // marginTop:15,
        height: '100%',
        width: '80%',
        // backgroundColor: 'green',
        // zIndex: 100,
    },
    thirdpage1: {
        // marginTop:15,
        height: '100%',
        width: '80%',
        // backgroundColor: 'red',
        // zIndex: 100,
    },
    firstpage2: {
        marginTop: 15,
        height: '0%',
        width: '80%',
        backgroundColor: 'cyan',

    },
    secondpage2: {
        // marginTop:15,
        height: '0%',
        width: '80%',
        backgroundColor: 'cyan',

    },
    thirdpage2: {
        // marginTop:15,
        height: '0%',
        width: '80%',
        backgroundColor: 'cyan',

    },
    ruler: {
        height: 20,
    },
    container: {
        // backgroundColor: '#d9f9b1',
        flexDirection: 'column',
        height: '100%',
        justifyContent: 'space-between',
    },
    tophead: {
        position: 'relative',
        top: 0,
        backgroundColor: '#7FCCF7',
        flexDirection: 'row',
        zIndex: 999,
        height: 50,
    },
    toptitle: {
        // marginLeft: 15,
        marginTop: 10,
        textAlign: 'center',
        fontSize: 25,
        color: 'white',
        width: '100%',
        marginLeft: -50,
        fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Avenir-Light',
    },
    topicon: {
        width: 50,
        marginTop: 15,
    },
}); 
